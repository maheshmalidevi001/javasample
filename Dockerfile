FROM  openjdk
WORKDIR /
COPY /target/dependency/webapp-runner.jar .
EXPOSE 8090
CMD ["java", "-jar", "webapp-runner.jar"]
